module sheldonAPI

go 1.20

require (
	github.com/golang-jwt/jwt/v5 v5.1.0 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/jmoiron/sqlx v1.3.5 // indirect
	github.com/lib/pq v1.10.9 // indirect
	github.com/mattn/go-sqlite3 v1.14.17 // indirect
	golang.org/x/crypto v0.15.0 // indirect
)

package router

import (
	"fmt"
	"sheldonAPI/internal/log"
	"sheldonAPI/internal/user"
	"time"

	"github.com/golang-jwt/jwt/v5"
)

var (
	// TODO: make this configurable ofc XD
	jwtSecret = "hokus_pokus_simsalabim_jak_ktos_to_przeczyta_to_bedzie_dym"
)

func GetJWT(u user.User) (string, error) {
	// https://auth0.com/docs/secure/tokens/json-web-tokens/json-web-token-claims
	// TODO: make signing method configurable
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"sub":  u.Email,                          // subject
		"iat":  time.Now().Unix(),                // issued at time
		"exp":  time.Now().Add(time.Hour).Unix(), // expiry time, TODO: make this configurable
		"role": u.Role,
	})

	signed, err := token.SignedString([]byte(jwtSecret))
	if err != nil {
		log.Logger.Error("Could not sign token", "cause", err)
		return "", err
	}
	return signed, nil
}

func VerifyJWT(toVerify string) (*jwt.Token, error) {
	token, err := jwt.Parse(toVerify, func(token *jwt.Token) (interface{}, error) {
		// Don't forget to validate the alg is what you expect:
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
		}

		return []byte(jwtSecret), nil
	})

	if err != nil {
		log.Logger.Error("Could not parse jwt", "cause", err)
		return &jwt.Token{}, err
	}
	log.Logger.Debug(fmt.Sprintf("Verified token: %+v", token))
	return token, nil
}

package router

import (
	"fmt"
	"net/http"
	"sheldonAPI/internal/log"
	"sheldonAPI/internal/user"
	"strings"

	"github.com/golang-jwt/jwt/v5"
	"github.com/gorilla/mux"
)

type Endpoint struct {
	methods      []string
	path         string
	handleFunc   func(http.ResponseWriter, *http.Request)
	rolesAllowed []user.UserRole
}

func (e *Endpoint) Register(r *mux.Router) {
	if len(e.rolesAllowed) == 0 {
		r.HandleFunc(e.path, e.handleFunc).Methods(e.methods...)
	} else {
		r.HandleFunc(e.path, e.handlerWithAuth()).Methods(e.methods...)
	}
}

func (e *Endpoint) handlerWithAuth() func(w http.ResponseWriter, r *http.Request) {
	// return a closure that can access e.handleFunc and e.rolesAllowed
	mainHandler, rolesAllowed := e.handleFunc, e.rolesAllowed
	return func(w http.ResponseWriter, r *http.Request) {
		h := r.Header.Get("Authorization")
		auth, found := strings.CutPrefix(h, "Bearer ")
		if !found {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		token, err := VerifyJWT(auth)
		if err != nil {
			// TODO: handle specific errors, e.g. invalid signature
			log.Logger.Error("Could not veryfy jwt", "cause", err)
			w.WriteHeader(http.StatusUnauthorized)
			return
		}
		if claims, ok := token.Claims.(jwt.MapClaims); ok {
			role, ok := claims["role"].(string)
			if !ok {
				log.Logger.Error("role is not a string", "token", token)
				w.WriteHeader(http.StatusUnauthorized)
			}

			allow := false
			for _, r := range rolesAllowed {
				if r == user.UserRole(role) {
					allow = true
					break
				}
			}
			if !allow {
				fmt.Fprintf(w, "You are not authorized to use this endpoint.")
				w.WriteHeader(http.StatusUnauthorized)
				return
			}
		} else {
			log.Logger.Error("Error when casting claims", "token", token)
			w.WriteHeader(http.StatusInternalServerError)
			return
		}
		// Authentication and authorization complete. Now we can handle the request.
		mainHandler(w, r)
	}
}

// Functional options pattern
func NewEndpoint(options ...func(*Endpoint)) *Endpoint {
	// default config - no roles required
	endpoint := Endpoint{
		rolesAllowed: []user.UserRole{},
	}
	for _, o := range options {
		o(&endpoint)
	}
	return &endpoint
}

func WithMethods(methods ...string) func(*Endpoint) {
	return func(e *Endpoint) {
		e.methods = append(methods, http.MethodOptions) // always add OPTIONS to methods, for cors preflight requests
	}
}

func WithPath(path string) func(*Endpoint) {
	return func(e *Endpoint) {
		e.path = path
	}
}

func WithHandleFunc(handleFunc func(http.ResponseWriter, *http.Request)) func(*Endpoint) {
	return func(e *Endpoint) {
		e.handleFunc = handleFunc
	}
}

func WithRoles(roles ...user.UserRole) func(*Endpoint) {
	return func(e *Endpoint) {
		e.rolesAllowed = roles
	}
}

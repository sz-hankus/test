package user

import (
	"time"
)

type UserRole string

var (
	Reserver   UserRole = "reserver"
	LabMaster  UserRole = "lab_master"
	Admin      UserRole = "admin"
	SuperAdmin UserRole = "super_admin"
)

type User struct {
	Id             uint64    `json:"id" db:"user_id"`
	Email          string    `json:"email" db:"email"`
	HashedPassword string    `json:"-" db:"password"`
	Role           UserRole  `json:"role" db:"role"`
	CreationTime   time.Time `json:"creationTime" db:"created_on"`
}

// What the user provides when they want to register
type UserForm struct {
	Email    string   `json:"email"`
	Password string   `json:"password"`
	Role     UserRole `json:"role"`
}

package controller

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"net/http"
	"sheldonAPI/internal/log"
	"sheldonAPI/internal/router"
	"sheldonAPI/internal/user"

	"github.com/gorilla/mux"
)

type UserHTTPController struct {
	Usecase user.UserUseCase
}

func (c *UserHTTPController) RegisterEndpoints(r *mux.Router) {
	sub := r.PathPrefix("/users").Subrouter()
	endpoints := []router.Endpoint{
		*router.NewEndpoint(
			router.WithMethods(http.MethodPost),
			router.WithPath(""),
			router.WithHandleFunc(c.RegisterUser),
		),
		*router.NewEndpoint(
			router.WithMethods(http.MethodPost),
			router.WithPath("/login"),
			router.WithHandleFunc(c.Login),
		),
		*router.NewEndpoint(
			router.WithMethods(http.MethodGet),
			router.WithPath("/{email}"),
			router.WithHandleFunc(c.GetUser),
			router.WithRoles(user.Reserver, user.LabMaster, user.Admin, user.SuperAdmin),
		),
		*router.NewEndpoint(
			router.WithMethods(http.MethodGet),
			router.WithPath(""),
			router.WithHandleFunc(c.GetUsers),
			router.WithRoles(user.Admin, user.SuperAdmin),
		),
	}

	log.Logger.Info("registering endpoints...")
	for _, e := range endpoints {
		e.Register(sub)
	}
}

func (c *UserHTTPController) GetUsers(w http.ResponseWriter, r *http.Request) {
	users, err := c.Usecase.GetUsers()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Logger.Error("Could not get users", "cause", err)
		// TODO: handle displaying errors in json or smth
		fmt.Fprint(w, err.Error())
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(users)
	if err != nil {
		log.Logger.Error("Could not encode to json", "cause", err)
		w.WriteHeader(http.StatusInternalServerError)
		// TODO: handle displaying errors in json or smth
		fmt.Fprint(w, err.Error())
		return
	}
}

func (c *UserHTTPController) GetUser(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	email := vars["email"]

	u, err := c.Usecase.GetUser(email)
	if err == sql.ErrNoRows {
		w.WriteHeader(http.StatusNotFound)
		fmt.Fprint(w, err.Error())
		return
	} else if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Fprint(w, err.Error())
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(u)
	if err != nil { // TODO: handle error somehow
		log.Logger.Error("Could not encode to json", "cause", err)
	}
}

func (c *UserHTTPController) RegisterUser(w http.ResponseWriter, r *http.Request) {
	var userParams user.UserForm
	// TODO: add json validation (check if it contains all the fields)
	err := json.NewDecoder(r.Body).Decode(&userParams)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}

	u, err := c.Usecase.CreateUser(userParams) // TODO: handle error when the user already exists
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	token, err := router.GetJWT(u)
	if err != nil {
		log.Logger.Error("Could not create jwt", "user", u, "cause", err)
		w.WriteHeader(http.StatusInternalServerError)
	}

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(map[string]interface{}{"jwt": token}) // TODO: maybe make this a type?
	if err != nil {
		log.Logger.Error("Could not encode jwt", "cause", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.WriteHeader(http.StatusOK)
}

func (c *UserHTTPController) Login(w http.ResponseWriter, r *http.Request) {
	var userParams user.UserForm
	// TODO: add json validation (check if it contains all the fields)
	err := json.NewDecoder(r.Body).Decode(&userParams)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprint(w, err.Error())
		return
	}

	u, valid, err := c.Usecase.Login(userParams.Email, userParams.Password)
	if err != nil {
		if err == user.ErrUserNotFound {
			w.WriteHeader(http.StatusNotFound)
		} else {
			log.Logger.Error("Error when trying to find and verify user", "cause", err)
			w.WriteHeader(http.StatusInternalServerError)
		}
		return
	} else if !valid {
		w.WriteHeader(http.StatusUnauthorized)
		return
	}

	token, err := router.GetJWT(u)

	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	err = json.NewEncoder(w).Encode(map[string]interface{}{"jwt": token}) // TODO: maybe make this a type?
	if err != nil {
		log.Logger.Error("Could not encode jwt", "cause", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

}

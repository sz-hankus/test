package user

import (
	"database/sql"
	"errors"
	"sheldonAPI/internal/log"
	"time"

	"golang.org/x/crypto/bcrypt"
)

var (
	ErrUserNotFound = errors.New("User not found")
)

type UserRepo interface {
	FindAll() ([]User, error)
	Find(email string) (User, error)
	Save(user User) (User, error)
	// Update(email string, new User) (User, error)
	// Delete(email string) (User, error)
}

type UserUseCase struct {
	Repo UserRepo
}

func (uc *UserUseCase) GetUsers() ([]User, error) {
	users, err := uc.Repo.FindAll()
	if err != nil {
		return []User{}, err
	}
	return users, nil
}

func (uc *UserUseCase) GetUser(email string) (User, error) {
	users, err := uc.Repo.Find(email)
	if err != nil {
		return User{}, err
	}
	return users, nil
}

func (uc *UserUseCase) CreateUser(req UserForm) (User, error) {
	hash, err := bcrypt.GenerateFromPassword([]byte(req.Password), 12) // TODO: make the cost configurable
	if err != nil {
		return User{}, err
	}
	var u User
	u.Email = req.Email
	u.HashedPassword = string(hash)
	u.Role = req.Role
	u.CreationTime = time.Now()

	created, err := uc.Repo.Save(u)
	if err != nil {
		log.Logger.Error("Could not save user to the database", "user", u, "cause", err)
		return User{}, err
	}
	return created, nil
}

func (uc *UserUseCase) Login(email, password string) (User, bool, error) {
	u, err := uc.Repo.Find(email)
	if err != nil {
		if err == sql.ErrNoRows {
			return u, false, ErrUserNotFound
		} else {
			return u, false, err
		}
	}
	err = bcrypt.CompareHashAndPassword([]byte(u.HashedPassword), []byte(password))
	if err != nil {
		return u, false, nil
	}
	return u, true, nil
}

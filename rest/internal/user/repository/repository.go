package repository

import (
	"database/sql"
	"fmt"
	"os"
	"sheldonAPI/internal/log"
	"sheldonAPI/internal/user"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

var (
	// TODO: make this configurable ofc XD
	connUrl = "postgres://admin:mysecretpassword@localhost:5432/sheldon?sslmode=disable"
	db      *sqlx.DB
)

func init() {
	log.Logger.Info(fmt.Sprintf("Database init started. Using url: %s", connUrl))
	base, err := sql.Open("postgres", connUrl)
	if err != nil {
		log.Logger.Error("Failed to access database", "cause", err)
		os.Exit(2) // TODO: maybe define different exit codes for different reasons
	}
	db = sqlx.NewDb(base, "postgres")
	err = db.Ping()
	if err != nil {
		log.Logger.Error("Failed to ping database", "cause", err)
		os.Exit(2)
	}
}

func CloseDB() {
	log.Logger.Info("Closing db...")
	err := db.Close()
	if err != nil {
		log.Logger.Error("Closing db failed. Oh hell nah.")
		return
	}
	log.Logger.Info("Db closed successfully. Yay.")
}

type UserPGRepo struct{}

// func main() {
// 	err := db.Ping()
// 	if err != nil {
// 		fmt.Print("ping err, ", err)
// 	}

// 	repo := UserPGRepo{}
// 	u, err := repo.Find("testingass")
// 	if err != nil {
// 		fmt.Println(err)
// 	}
// 	fmt.Println("foound, ", u)
// 	// user, err := repo.Save(user.User{Id: 123, Email: "testinga", Password: "123", Role: "reserver", CreationTime: time.Now()})
// 	// if err != nil {
// 	// 	fmt.Println(err)
// 	// }
// 	// fmt.Println(user)

// 	users, err := repo.FindAll()
// 	if err != nil {
// 		fmt.Println("find err: ", err)
// 	}
// 	fmt.Println(users)
// 	CloseDB()
// }

func (r *UserPGRepo) Save(u user.User) (user.User, error) {
	s := `
	INSERT INTO users
	(email, password, role, created_on) VALUES 
	(:email, :password, :role, :created_on)
	RETURNING user_id;`
	stmt, err := db.PrepareNamed(s)
	if err != nil {
		return user.User{}, err
	}
	var id uint
	err = stmt.Get(&id, u)
	if err != nil {
		return user.User{}, err
	}
	u.Id = uint64(id)
	log.Logger.Info("Saved user to database", "user", u)
	return u, nil
}

func (r *UserPGRepo) FindAll() ([]user.User, error) {
	s := `
	SELECT * FROM users;
	`
	users := []user.User{}
	err := db.Select(&users, s)
	if err != nil {
		return []user.User{}, err
	}
	return users, nil
}

func (r *UserPGRepo) Find(email string) (user.User, error) {
	s := `
	SELECT * FROM users
	WHERE email=$1;
	`
	var u user.User
	err := db.QueryRowx(s, email).StructScan(&u)
	if err != nil {
		return user.User{}, err
	}
	return u, nil
}

package main

import (
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"sheldonAPI/internal/log"
	"sheldonAPI/internal/router"
	"sheldonAPI/internal/user"
	controller "sheldonAPI/internal/user/http"
	"sheldonAPI/internal/user/repository"
	"syscall"
)

func shutdown(cause string, exitCode int) {
	log.Logger.Info("Shutting down...", "cause", cause)
	// free resources etc.
	repository.CloseDB()
	os.Exit(exitCode)
}

func handleSignal(received os.Signal) {
	log.Logger.Info("Received signal", "signal", received)
	switch received {
	case syscall.SIGINT:
		shutdown(fmt.Sprintf("received signal %s", received), 1)
	case syscall.SIGTERM:
		shutdown(fmt.Sprintf("received signal %s", received), 1)
	case syscall.SIGKILL:
		shutdown(fmt.Sprintf("received signal %s", received), 1)
	}
}

func initSignals() {
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGKILL)
	received := <-sigs
	handleSignal(received)
}

func main() {
	log.Logger.Info("Starting app...")
	go initSignals()

	repository := repository.UserPGRepo{}
	usecase := user.UserUseCase{Repo: &repository}
	controller := controller.UserHTTPController{Usecase: usecase}
	r := router.NewRouter(&controller)

	// TODO: create a config mechanism (probably from env variables)
	log.Logger.Info("Starting http server on port 8080...")
	err := http.ListenAndServe(":8080", r)
	if err != nil {
		log.Logger.Error("Server failed to start.", "cause", err)
	}
}

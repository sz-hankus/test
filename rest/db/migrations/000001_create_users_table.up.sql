BEGIN;

do $$
BEGIN
	IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'user_role') THEN
	CREATE TYPE user_role AS ENUM (
		'reserver',
		'lab_master',
		'admin',
		'super_admin'
	);
	END IF;
END
$$;



CREATE TABLE IF NOT EXISTS users(
	user_id serial PRIMARY KEY,
	email VARCHAR (100) UNIQUE NOT NULL,
	password VARCHAR (100) NOT NULL,
	role user_role NOT NULL,
	created_on TIMESTAMP NOT NULL
);

COMMIT;

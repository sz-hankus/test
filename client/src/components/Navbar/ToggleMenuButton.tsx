import React from "react";

interface ToggleMenuButtonProps {
  isMenuOpen: boolean;
  toggleMenu: () => void;
}

const ToggleMenuButton: React.FC<ToggleMenuButtonProps> = ({
  isMenuOpen,
  toggleMenu,
}) => {
  return (
    <button
      type="button"
      onClick={toggleMenu}
      className="font-medium hover:text-blue-700 md:hidden"
    >
      {/* ikonki z wzięte z https://heroicons.com/micro, nie wiem jak tam kwestia z licenjami na takie rzeczy */}
      {isMenuOpen ? (
        <svg
          xmlns="http://www.w3.org/2000/svg"
          viewBox="0 0 16 16"
          fill="currentColor"
          className="w-8 h-8"
        >
          <path d="M5.28 4.22a.75.75 0 0 0-1.06 1.06L6.94 8l-2.72 2.72a.75.75 0 1 0 1.06 1.06L8 9.06l2.72 2.72a.75.75 0 1 0 1.06-1.06L9.06 8l2.72-2.72a.75.75 0 0 0-1.06-1.06L8 6.94 5.28 4.22Z" />
        </svg>
      ) : (
        <svg
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 24 24"
          strokeWidth={1.5}
          stroke="currentColor"
          className="w-8 h-8"
        >
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            d="M3.75 6.75h16.5M3.75 12h16.5m-16.5 5.25h16.5"
          />
        </svg>
      )}
    </button>
  );
};

export default ToggleMenuButton;

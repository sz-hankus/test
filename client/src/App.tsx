import "./App.css";
import Register from "./components/Register.tsx";
import Login from "./components/Login.tsx";
import { Route, Routes } from "react-router-dom";
import RequireAuth from "./components/RequireAuth.tsx";
import Index from "./components/Index.tsx";
import User from "./components/User.tsx";
import Navbar from "./components/Navbar/Navbar.tsx";
import Users from "./components/Users.tsx";

function App() {
  return (
    <>
      <Navbar />
      <Routes>
        <Route path="/" element={<Index />} />

        <Route path="/users/register" element={<Register />} />
        <Route path="/users/login" element={<Login />} />

        <Route element={<RequireAuth />}>
          <Route path="/users/:email" element={<User />} />
          <Route path="/users" element={<Users />} />
        </Route>
      </Routes>
    </>
  );
}

export default App;

import axios from "../api/apiConfig.ts";
import { UserForm } from "../models/User.ts";

export const getUser = async (email: string, jwtAccessToken: string) => {
  try {
    const response = await axios.get(`/users/${email}`, {
      headers: { Authorization: `Bearer ${jwtAccessToken}` },
    });
    return response.data;
  } catch (err) {
    console.log(err);
    throw err;
  }
};

export const loginUser = async (credentials: UserForm) => {
  try {
    const response = await axios.post(
      "/users/login",
      JSON.stringify(credentials),
      { headers: { "Content-Type": "application/json" } }
    );
    return response.data;
  } catch (err) {
    console.log(err);
    throw err;
  }
};

export const registerUser = async (credentials: UserForm) => {
  try {
    await axios.post("/users", JSON.stringify(credentials), {
      headers: { "Content-Type": "application/json" },
    });
  } catch (err) {
    console.log(err);
    throw err;
  }
};
